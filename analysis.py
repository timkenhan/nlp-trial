from nltk.corpus import brown

from trie import trie

import pickle
import re


#these are the output paths
outFile_Fiction = "fiction.json"
outFile_NonFiction = "nonfiction.json"


#split into fiction/non-fiction subsets

news = brown.words(categories='news')
editorial = brown.words(categories='editorial')
reviews = brown.words(categories='reviews')
religion = brown.words(categories='religion')
hobbies = brown.words(categories='hobbies')
lore = brown.words(categories='lore')
belles_lettres = brown.words(categories='belles_lettres')
government = brown.words(categories='government')
learned = brown.words(categories='learned')

listNonFiction = [news, editorial, reviews, hobbies, lore, belles_lettres, government, learned]


fiction = brown.words(categories='fiction')
mystery = brown.words(categories='mystery')
science_fiction = brown.words(categories='science_fiction')
adventure = brown.words(categories='adventure')
romance = brown.words(categories='romance')
humor = brown.words(categories='humor')

listFiction = [fiction, mystery, science_fiction, adventure, romance, humor]


lengthNonFiction = 0
for corpus in listNonFiction:
    lengthNonFiction += len(corpus)

lengthFiction = 0
for corpus in listFiction:
    lengthFiction += len(corpus)


rootNonFiction = trie("",0)
rootFiction = trie("",0)


isWord = re.compile('[A-Za-z.]')

# add each trigram, bigram, and unigram frequency to the trees
for corpus in listNonFiction:
    corpus = [x for x in corpus if isWord.match(x)]
    length = len(corpus)
    for i in range(2, length):
        toAdd = [corpus[i-2], corpus[i-1], corpus[i]]
        rootNonFiction.add(toAdd)

for corpus in listFiction:
    corpus = [x for x in corpus if isWord.match(x)]
    length = len(corpus)
    for i in range(2, length):
        toAdd = [corpus[i-2], corpus[i-1], corpus[i]]
        rootFiction.add(toAdd)


rootNonFiction.computeProbabilities()
rootFiction.computeProbabilities()


f = open(outFile_Fiction, 'wb')
pickle.dump(rootFiction, f)
f.close()

f = open(outFile_NonFiction, 'wb')
pickle.dump(rootNonFiction,f)
f.close()



