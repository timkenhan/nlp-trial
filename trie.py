class trie:
    """ basic radix tree class equipped to compute probability of all 1-,..,n-grams
    does not yet handle punctuation"""
    
    count = 0 #simple count of the occurances of words at each level of ancestry
    depth = 0 
    word = ""
    children = []
    
    def __init__(self, wordIn, depthIn):
        self.count = 0
        self.depth = depthIn
        self.word = wordIn
        self.children = []
        self.children_by_word = dict()
        self.probability = 0
        
    def add(self, relations): # relations is a list of ordered ancestors with the child greatest
        self.count += 1
        current = self
        sizeOfGram = len(relations)
        i = 0
        for strg in relations:
            if strg in current.children_by_word:
                node = current.children_by_word[strg]
                node.count += 1
                #print(node.word + u" " + str(node.count))
                current = node
            else:
                newNode = trie(strg, i+1)
                current.children.append(newNode)
                newNode.count += 1
                current.children_by_word[newNode.word] = newNode
                #print(newNode.word + u" " + str(newNode.count))
                current = newNode
            i += 1

    def computeProbabilities(self):
        for node in self.children:
            node.probability = node.count/float(self.count) #watch out for underflow with large data
            node.computeProbabilities()

    def display(self, node):
        for child in node.children:
            print(child.word + " " + str(child.count) + " " + str(child.probability))
            self.display(child)
