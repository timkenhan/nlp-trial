import trie
import random

def random_word(root, previous_words):
    """ Takes the root of the tree, and a list of previous words.
    For example, random_word(root, ["this","is"]) returns a random word that
    can appear after the words "this is".
    Raises an exception (currently KeyError) if the n-gram doesn't appear. """
    node = root
    for word in previous_words:
        node = node.children_by_word[word]
    r = random.randint(0, node.count-1)
    so_far = 0
    for child in node.children:
        if r < so_far + child.count:
            return child.word
        so_far += child.count
    assert False


def random_words(root, n, previous_words = None, words_to_generate = None):
    """ Generates a list of words (returns a generator).  Arguments:
        root: The tree to generate from
        n: The size of n-gram to use
        previous_words (optional): The words before the words to generate
        words_to_generate (optional): The number of words to generate;
            if this is None, generates an infinite list.
        
        Use list(random_words(root, n, [], number of words)) to get this as
        a list.
        
        If an n-gram doesn't exist, then it tries a shorter string (although
        I think this shouldn't happen?).
    """
    if previous_words == None:
        previous_words = []
    while True:
        try:
            while len(previous_words) > 0 and previous_words[-1] not in root.children_by_word:
                previous_words.pop()
            while len(previous_words) >= n:
                previous_words.pop(0)
            word = random_word(root, previous_words)
            yield word
            previous_words += [word]
            if words_to_generate != None:
                words_to_generate -= 1
                if words_to_generate <= 0:
                    return
        except KeyError:
            # try with one fewer words
            previous_words.pop(0)


test_tree = trie.trie('', 0)
test_tree.add(['this','is','the'])
test_tree.add(['this','is','the'])
test_tree.add(['this','is','an'])
test_tree.add(['this','can','be'])
test_tree.add(['that','is','the'])
test_tree.add(['is','the','big'])
test_tree.add(['is','the','best'])

