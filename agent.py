from randword import random_words
import pickle

with open("fiction.json","rb") as pickle_file:
        lexicon = pickle.load(pickle_file)

def genResponse(sentence, n=3, length=5):
	return random_words(lexicon, n, sentence, length)

prompt = "Hello!"

def process(s):
	return s.split()

def deprocess(ss):
	ret = ""
	for s in ss:
		ret += s
		ret += " "
	return ret + "."

while True:
	sentence = input(deprocess(prompt))
	if sentence == "Exit":
		break
	prompt = list(genResponse(process(sentence)))
